﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateMachine : MonoBehaviour
{
    #region Parameters
    public State initialState;
    public State currentState;
    #endregion
    #region Functions
    public void Start()
    {
        currentState = initialState;
    }
    public void ChangeState(State _NextState)
    {
        currentState.enabled = false;
        currentState = _NextState;
        currentState.enabled = true;
    }
    #endregion
}
