﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class State : MonoBehaviour
{
    // Distancias y conteo de enemigos
    //Distancia y deteccion de enemigos
    //Comprobación de muerte
    public float distance;
    public float minDistance;
    public float enemiesNumber;
    public bool death;
    public State DeathState;

    public virtual void CheckDistance(Vector3 _initial, Vector3 _final)
    {
        Vector3 _distance = _final - _initial;
        distance = _distance.magnitude;
    }
    public virtual void CheckEnemies( float _minDistance)
    {
        /*
         for(int = 0; i<allies.length; i++){
         CheckDistance(gameobject.Transform.position, allies[i].Transform.position);
         if(distance < minDistance) enemiesNumber += 1;
         */
    }
    public virtual void CheckDeath()
    {
        if (death)
        {
            /*anim.SetTrigger("Death");
             * StateMachine.ChangeState(DeathState);
             * */
        }
    }
}
